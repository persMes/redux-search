import jsonPlaceholder from "../apis/jsonPlaceholder";

export const fetchPosts = () => async dispatch => {
  const response = await jsonPlaceholder.get("/posts");

  dispatch({ type: "FETCH_POSTS", payload: response.data });
};

export const fetchUser = id => async dispatch => {
  const response = await jsonPlaceholder.get("/users/" + id);

  dispatch({ type: "FETCH_USER", payload: response.data });
};

export const filterPosts = (value, data) => async dispatch => {
  const load = data.filter(item => item.body.includes(value));

  dispatch({
    type: "FILTER_POSTS",
    payload: load
  });
};
