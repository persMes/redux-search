import React from "react";
import "./SearchBar.css";
import { connect } from "react-redux";
import { filterPosts } from "../actions";

class SearchBar extends React.Component {
  componentDidUpdate() {
    this.props.filterPosts("", this.props.posts);
  }

  onFormSubmit = event => {
    event.preventDefault();

    const value = document.getElementById("searchBar").value;
    console.log(document.getElementById("searchBar").value);

    this.props.filterPosts(value, this.props.posts);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onFormSubmit}>
          <div className="field">
            <input
              className="searchBar"
              type="text"
              placeholder="Vyhledávání"
              id="searchBar"
            />
          </div>
        </form>
        <p />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.posts
  };
};

export default connect(
  mapStateToProps,
  { filterPosts }
)(SearchBar);
