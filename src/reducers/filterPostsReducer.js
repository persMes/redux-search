export default (state = [], action) => {
  switch (action.type) {
    case "FILTER_POSTS":
      return action.payload;
    default:
      return state;
  }
};
