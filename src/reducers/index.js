import { combineReducers } from "redux";
import postsReducer from "./postReducer";
import usersReducer from "./usersReducer";
import filterPostsReducer from "./filterPostsReducer";

export default combineReducers({
  posts: postsReducer,
  users: usersReducer,
  filterPosts: filterPostsReducer
});
